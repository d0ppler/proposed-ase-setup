var gulp   = require( 'gulp' ),
    server = require( 'gulp-develop-server' );

// run server
gulp.task( 'server:start', function() {
    server.listen( { path: './src/server/server.js' } );
});

// restart server if app.js changed
gulp.task( 'server:dev',['server:start'], function() {
    gulp.watch( [ './src/server/server.js' ], server.restart );
});