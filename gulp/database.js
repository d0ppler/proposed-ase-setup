var gulp = require( 'gulp' );
var shell = require('gulp-shell')
var path = require('path');

var models = require(path.join(__dirname , "/../src/server/models"));
var db = require(path.join(__dirname , "/../src/server/models/index.js"));

var basePath = path.join(__dirname, "/../src/server/");
var configPath = path.join(basePath, "/config/config.js");
var seedersPath = path.join(basePath, "/seeders");
var sequelizePath = path.join(__dirname, "/../node_modules/sequelize-cli/bin/sequelize");

//undoing all seeds
gulp.task('database:drop-data', shell.task([
	'node ' + sequelizePath + ' --config ' + configPath + ' --seeders-path ' + seedersPath + ' db:seed:undo:all'
]));

//loads all defined models und creates the appropriate database tables
gulp.task('database:sync', ['database:drop-data'], function(){

	models.sequelize.sync({force: true}).then(function(){
		console.log("Tabels created");
	});
});

//depends on an fresh created database
//runs the seeders to fill the database with demo data
gulp.task('database:rebuild', ['database:sync'], shell.task([
	'node ' + sequelizePath + ' --config ' + configPath + ' --seeders-path ' + seedersPath + ' db:seed'
]));