var router = require('koa-router')();

require('./userService').register(router);

module.exports = router;