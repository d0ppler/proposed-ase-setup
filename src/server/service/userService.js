var models = require('../models');

var show = exports.show = function *show(){
	
	var user = yield models.user.findById(Number(this.params.userId));
		
	this.body = user;
}

var create = exports.create = function *create(){
	this.body = "user created";
}

exports.register = function(router){
	router.get('/users/:userId', show);
	router.post('/users');
}