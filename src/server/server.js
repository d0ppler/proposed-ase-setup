var serve = require('koa-static');
var app = require('koa')();
var router = require('./service/index.js');


//setting the webroot - for static file serving
app.use(serve(__dirname + '/../client/'));

//setting routes - defined by the service layer
app.use(router.routes());

//starting server
app.listen(8080);
console.log("Server started on port 8080");